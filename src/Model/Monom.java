package Model;

public class Monom {
    private double coef;
   private int exp;

public Monom(double coef, int exp){
    this.exp=exp;
    this.coef=coef;
}

public int getExp() {
	return exp;
}
public void setExp(int exp) {
	this.exp=exp;
}

public double getCoef() {
	return coef;
}
public void setCoef(double coef) {
	this.coef=coef;
}
    @Override
    public String toString(){
String s="" ;

if(this.coef!=0){
    if (this.exp==0) {
        s= this.coef + " ";
    }
    else{
        s= this.coef + "x^" + this.exp + "+";
    }
    }
    return s;

   }
   }
