package Model;
import java.util.*;

public class Polinom {
    public ArrayList<Monom> termeni = new ArrayList<>();

    public Polinom() {
        termeni = new ArrayList<Monom>();
    }
    public void adaugaTermeni(Monom t){
        boolean ok=false;
        for (Monom m : termeni) {
            if (t.getExp() == m.getExp()) {
                m.setCoef(m.getCoef() + t.getCoef());
                ok = true;
                break;
            }
        }
            if (ok!=true){
            termeni.add(t);

        }

        }
        @Override
public String toString(){
        String s="";
            for (Monom m:termeni){
                s= s + m.toString();
            }
            return s;

            }
public Polinom copiere(){
        Polinom p=new Polinom();

        for(Monom m:termeni) {
            p.adaugaTermeni(new Monom(m.getCoef(), m.getExp()));
        }
        return p;

        }
        }

