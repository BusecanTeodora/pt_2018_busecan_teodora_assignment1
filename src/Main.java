
import Logic.Operatii;
import Model.Monom;
import Model.Polinom;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Polinom t1 = new Polinom();
		Polinom t2 = new Polinom();
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		
		t1.adaugaTermeni(new Monom(2,3));
		t1.adaugaTermeni(new Monom(1,2));
		t2.adaugaTermeni(new Monom(4,3));
		t2.adaugaTermeni(new Monom(2,2));
		
		p1.adaugaTermeni(new Monom(1,4));
		p1.adaugaTermeni(new Monom(-1,3));
		p1.adaugaTermeni(new Monom(2,2));
		p1.adaugaTermeni(new Monom(-1,1));
		p1.adaugaTermeni(new Monom(3,0));
		p2.adaugaTermeni(new Monom(1,1));
		p2.adaugaTermeni(new Monom(-1,0));
		
		Polinom sum = Operatii.Adunare(t1, t2);
		Polinom sub = Operatii.Scadere(t1, t2);
		Polinom inm = Operatii.Inmultire(t1, t2);
		Polinom deriv = Operatii.Derivare(t1);
		Polinom integ = Operatii.Integrare(t1);
		//Polinom div = Operatii.divOperation(p1,p2);
		
		System.out.println("suma este " + sum + "\n" + "scaderea este " + sub + "\n" + "Produsul este " + inm + "\n" + "Derivarea este " + deriv + "\n" + "Integrarea este " + integ);
		//System.out.println("\n Catul este: " + div  );
		
		
	}

}
