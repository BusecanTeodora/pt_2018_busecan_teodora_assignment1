package Logic;
import Model.Monom;
import Model.Polinom;


public class Operatii {
    
    
    public static Polinom Adunare(Polinom p1, Polinom p2){
        Polinom p3=p2.copiere();
        for (Monom m:p1.termeni){
            p3.adaugaTermeni(m);
            }
            return p3;
    }
    
    public static Polinom Scadere(Polinom p1, Polinom p2) {
    	Polinom p3=p1.copiere();
    	Polinom p4=p2.copiere();
    	for(Monom m:p4.termeni) {
    		m.setCoef(m.getCoef()*(-1));
    		p3.adaugaTermeni(m);
    	}
    	return p3;
    }
    
   public static Polinom Inmultire(Polinom p1, Polinom p2) {
	   Polinom p3=new Polinom();
	   for(Monom m:p1.termeni) {
		   for(Monom n:p2.termeni) {
			   p3.adaugaTermeni(new Monom(m.getCoef()*n.getCoef(),m.getExp()+n.getExp()));
		   }
	   }
	   return p3;
   }
   
   public static Polinom Derivare(Polinom p1){
		Polinom p3 = new Polinom();
		
		for(Monom m:p1.termeni){
			p3.adaugaTermeni(new Monom(m.getCoef()*m.getExp(),m.getExp() -1));
		}
		
		return p3;
   }
   public static Polinom Integrare(Polinom p1) {
	   Polinom p3=new Polinom();
	   for(Monom m:p1.termeni) {
		   p3.adaugaTermeni(new Monom(m.getCoef()/m.getExp()+1,m.getExp()+1));   
	   }
	   return p3;
   }
	
   
   
    		
    		
    	
    	
    
}
