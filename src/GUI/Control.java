package GUI;

import java.awt.event.ActionEvent;
import java.awt.*;
import java.security.*;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;
//import java.lang.*;
import Logic.Operatii;
import Model.Monom;
import Model.Polinom;

public class Control {

	private Vedere view;
	private String result;
	private String rest;

	public Control(Vedere view) {
		this.view = view;
		setupControl();
	}

	public void setupControl() {
		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				

				if (e.getSource().equals(view.b)) {
					
					String input = view.tf.getText();
					String input1 = view.tf1.getText();
					
					if(input1.equals("")||input.equals("")){
						updateError();
						
					}
					else
					{
					
					Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
					Matcher m = p.matcher( input );
					Matcher n = p.matcher( input1 );
					Polinom p1 = new Polinom();
					Polinom p2 = new Polinom();
					while (m.find()){
					    
					    p1.adaugaTermeni(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
					}
					
					while(n.find()){
						p2.adaugaTermeni(new Monom(Double.parseDouble(n.group(1)),Integer.parseInt(n.group(2))));
					}
					
					Polinom t3 = Operatii.Adunare(p1,p2);
					
					result = t3.toString();
					rest = "";
					result =  result.substring(0, result.length()-2);
					 //rest = "";
					}
				}
				
				if(e.getSource().equals(view.b1)){
					String input = view.tf.getText();
					String input1 = view.tf1.getText();
					
					if(input1.equals("")||input.equals("")){
						updateError();
						
					}
					else
					{
					
					Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
					Matcher m = p.matcher( input );
					Matcher n = p.matcher( input1 );
					Polinom p1 = new Polinom();
					Polinom p2 = new Polinom();
					while (m.find()){
					    
					    p1.adaugaTermeni(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
					}
					
					while(n.find()){
						p2.adaugaTermeni(new Monom(Double.parseDouble(n.group(1)),Integer.parseInt(n.group(2))));
					}
					
					Polinom t3 = Operatii.Scadere(p1,p2);
					
					result = t3.toString();
					rest = "";
					result =  result.substring(0, result.length()-2);
					}
			
				}
				
				if(e.getSource().equals(view.b2)){
					
					String input = view.tf.getText();
					String input1 = view.tf1.getText();
					
					if(input1.equals("")||input.equals("")){
						updateError();
						
					}
					else
					{
					
					Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
					Matcher m = p.matcher( input );
					Matcher n = p.matcher( input1 );
					Polinom p1 = new Polinom();
					Polinom p2 = new Polinom();
					while (m.find()){
					    
					    p1.adaugaTermeni(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
					}
					
					while(n.find()){
						p2.adaugaTermeni(new Monom(Double.parseDouble(n.group(1)),Integer.parseInt(n.group(2))));
					}
					
					Polinom t3 = Operatii.Inmultire(p1,p2);
					
					result = t3.toString();
					rest = "";
					result =  result.substring(0, result.length()-2);
					}
				}
				
				if(e.getSource().equals(view.b3)){
					String input = view.tf.getText();
					String input1 = view.tf1.getText();
					
					if(input1.equals("")||input.equals("")){
						updateError();
						
					}
					else
					{
					/*
					Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
					Matcher m = p.matcher( input );
					Matcher n = p.matcher( input1 );
					Polynom p1 = new Polynom();
					Polynom p2 = new Polynom();
					while (m.find()){
					    
					    p1.addTerm(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
					}
					
					while(n.find()){
						p2.addTerm(new Monom(Double.parseDouble(n.group(1)),Integer.parseInt(n.group(2))));
					}
					
					Polynom t3 = Operation.divOperation(p1,p2);
					
					result = t3.toString();
					 rest = Operation.getRest();
					result =  result.substring(0, result.length()-2);
					}
			
				
				if(e.getSource().equals(view.b4)){
					String input = view.tf.getText();
					
					if(input.equals("")){ 
						updateError();
						
					}
					else
					{*/
					
					
					Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
					Matcher m = p.matcher( input );
					
					Polinom p1 = new Polinom();
					
					while (m.find()){
					    
					    p1.adaugaTermeni(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
					}
					
					
					
					Polinom t3 = Operatii.Derivare(p1);
					
					result = t3.toString();
					rest = "";
					result =  result.substring(0, result.length()-2);
					}
				}
				
				if(e.getSource().equals(view.b5)){
					String input = view.tf.getText();
					
					if(input.equals("")){
						updateError();
						
					}
					else
					{
					
					Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
					Matcher m = p.matcher( input );
					
					Polinom p1 = new Polinom();
					
					while (m.find()){
					    
					    p1.adaugaTermeni(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
					}
					
					
					
					Polinom t3 = Operatii.Integrare(p1);
					
					result = t3.toString();
					rest = "";
					result =  result.substring(0, result.length()-2);
					}
				}
				updateResult(result,rest);
			}
		};
		view.b.addActionListener(al);
		view.b1.addActionListener(al);
		view.b2.addActionListener(al);
		view.b3.addActionListener(al);
		view.b4.addActionListener(al);
		view.b5.addActionListener(al);
	}

	private void updateResult(String result, String rest) {
		view.tf3.setText(result + " ");
		
		view.tf2.setText(rest + " ");
	}
	
	
	private void updateError(){
		String result = "ERROR!";
		view.tf3.setText(result);
	}

	public static void main(String[] args) {
		Vedere view = new Vedere();
		Control c = new Control(view);

	}
}
