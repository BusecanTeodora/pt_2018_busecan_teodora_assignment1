package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class Vedere {
	
	
	JPanel p3 = new JPanel();
	JPanel p4 = new JPanel();
	JTextField tf = new JTextField(15);
	JTextField tf1 = new JTextField(15);
	JTextField tf2 = new JTextField(15);
	JTextField tf3 = new JTextField(15);
	JButton b = new JButton(" + ");
	JButton b1 = new JButton(" - ");
	JButton b2 = new JButton(" * ");
	JButton b3 = new JButton(" / ");
	JButton b4 = new JButton(" y' ");
	JButton b5 = new JButton(" ∫ ");
	
	
	public Vedere(){
		JFrame frame = new JFrame("Example");
		JFrame.setDefaultLookAndFeelDecorated(true);
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p5 = new JPanel();
		JLabel label = new JLabel("Polinom 1: ");
		JLabel label1 = new JLabel("Polinom 2: ");
		JLabel l = new JLabel("Rezultat: ");
		JLabel rest = new JLabel("Rest: ");
		
		BoxLayout box = new BoxLayout(p,BoxLayout.Y_AXIS);
		
		frame.setResizable(false);
		

		p.setLayout(box);
		p.setBorder(new EmptyBorder(new Insets(50, 200, 50, 200)));
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.add(p4);
		p.add(p5);
		
		p1.add(label);
		p1.add(tf);
		p1.add(label1);
		p1.add(tf1);
		
		Dimension expectedDimension = new Dimension(300, 100);
	    p1.setPreferredSize(expectedDimension);
	    p1.setMaximumSize(expectedDimension);
	    p1.setMinimumSize(expectedDimension);
	        
		p2.add(b);
		p2.add(Box.createRigidArea(new Dimension(0, 100)));
		p2.add(b1);
		p2.add(Box.createRigidArea(new Dimension(0, 100)));
		
		

        p2.setPreferredSize(expectedDimension);
        p2.setMaximumSize(expectedDimension);
        p2.setMinimumSize(expectedDimension);
        
		p3.add(b2);
		p3.add(Box.createRigidArea(new Dimension(0, 100)));
		p3.add(b3);
		p3.add(Box.createRigidArea(new Dimension(0, 100)));
		
		p3.setPreferredSize(expectedDimension);
	    p3.setMaximumSize(expectedDimension);
	    p3.setMinimumSize(expectedDimension);
	    
	    p4.add(b4);
	    p4.add(Box.createRigidArea(new Dimension(0, 100)));
	    p4.add(b5);
	    p4.add(Box.createRigidArea(new Dimension(0, 100)));
	    
	    p4.setPreferredSize(expectedDimension);
	    p4.setMaximumSize(expectedDimension);
	    p4.setMinimumSize(expectedDimension);
	    
	    p5.add(l);
		p5.add(tf3);
		p5.add(rest);
		p5.add(tf2);
		
		p5.setPreferredSize(expectedDimension);
	    p5.setMaximumSize(expectedDimension);
	    p5.setMinimumSize(expectedDimension);
	
		b.setSize(20,20);
		b1.setSize(20,20);
		b2.setSize(20,20);
		b3.setSize(20,20);
		b4.setSize(20,20);
		b5.setSize(20,20);
		
		
		b.setFont(new Font("Arial", Font.PLAIN, 50));
		b1.setFont(new Font("Arial", Font.PLAIN, 50));
		b2.setFont(new Font("Arial", Font.PLAIN, 50));
		b3.setFont(new Font("Arial", Font.PLAIN, 50));
		b4.setFont(new Font("Arial", Font.PLAIN, 50));
		b5.setFont(new Font("Arial", Font.PLAIN, 50));
		
		JPanel p6 = new JPanel();
		 Label l1 = new Label("Observatie!!");
		 Label l2 = new Label(" Ex pol: \n 2x^3 + 2x^2 + 4x^1 + 1x^0 ");
		
		
		 p6.add(l1); 
		 p6.add(l2);
		 
		Dimension dimi = new Dimension(500, 100);
		p6.setPreferredSize(dimi);
		p6.setMaximumSize(dimi);
		p6.setMinimumSize(dimi);
		p.add(p6);
		frame.add(p);
		
		
		
		
		
		
		frame.pack();
		frame.setSize(600,700);
	    
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
	
		 
		
		
		
		
		
	
		
	}

}
